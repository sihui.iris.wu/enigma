package enigma;

import static enigma.EnigmaException.*;

/** Class that represents a rotating rotor in the enigma machine.
 *  @author Iris Wu
 */
class MovingRotor extends Rotor {
    /** Position of notches. */
    private String _notches;

    /** A rotor named NAME whose permutation in its default setting is
     *  PERM, and whose notches are at the positions indicated in NOTCHES.
     *  The Rotor is initially in its 0 setting (first character of its
     *  alphabet).
     */
    MovingRotor(String name, Permutation perm, String notches) {
        super(name, perm);
        _notches = notches;
    }

    @Override
    boolean rotates() {
        return true;
    }

    @Override
    boolean atNotch() {
        for (int i = 0; i < _notches.length(); i++) {
            char curr = _notches.charAt(i);
            char alph = alphabet().toChar(setting());
            if (curr == alph) {
                return true;
            }
        }
        return false;
    }

    @Override
    void advance() {
        super.set(permutation().wrap(setting() + 1));
    }
}
