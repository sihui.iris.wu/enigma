# Enigma



## Project Description

This project mimics the incryption and decryption processes used in Enigma machines. This project is done during the course of CS61B-Data Structures in Fall 2021 semester, taught by Professor Paul Hilfinger in U.C. Berkeley. The complete project description can be found here: https://inst.eecs.berkeley.edu/~cs61b/fa21/materials/proj/proj1/index.html
