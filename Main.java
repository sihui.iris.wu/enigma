package enigma;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

import static enigma.EnigmaException.*;

/** Enigma simulator.
 *  @author Iris Wu
 */
public final class Main {
    /** Alphabet used in this machine. */
    private Alphabet _alphabet;

    /** Source of input messages. */
    private Scanner _input;

    /** Source of machine configuration. */
    private Scanner _config;

    /** File for encoded/decoded messages. */
    private PrintStream _output;

    /** An ArrayList containing all rotors that can be used. */
    private ArrayList<Rotor> _allRotors = new ArrayList<>();

    /** Process a sequence of encryptions and decryptions, as
     *  specified by ARGS, where 1 <= ARGS.length <= 3.
     *  ARGS[0] is the name of a configuration file.
     *  ARGS[1] is optional; when present, it names an input file
     *  containing messages.  Otherwise, input comes from the standard
     *  input.  ARGS[2] is optional; when present, it names an output
     *  file for processed messages.  Otherwise, output goes to the
     *  standard output. Exits normally if there are no errors in the input;
     *  otherwise with code 1. */
    public static void main(String... args) {
        try {
            new Main(args).process();
            return;
        } catch (EnigmaException excp) {
            System.err.printf("Error: %s%n", excp.getMessage());
        }
        System.exit(1);
    }

    /** Check ARGS and open the necessary files (see comment on main). */
    Main(String[] args) {
        if (args.length < 1 || args.length > 3) {
            throw error("Only 1, 2, or 3 command-line arguments allowed");
        }

        _config = getInput(args[0]);
        if (args.length > 1) {
            _input = getInput(args[1]);
        } else {
            _input = new Scanner(System.in);
        }

        if (args.length > 2) {
            _output = getOutput(args[2]);
        } else {
            _output = System.out;
        }
    }

    /** Return a Scanner reading from the file named JNAME. */
    private Scanner getInput(String jname) {
        try {
            return new Scanner(new File(jname));
        } catch (IOException excp) {
            throw error("could not open %s", jname);
        }
    }

    /** Return a PrintStream writing to the file named KNAME. */
    private PrintStream getOutput(String kname) {
        try {
            return new PrintStream(new File(kname));
        } catch (IOException excp) {
            throw error("could not open %s", kname);
        }
    }

    /** Configure an Enigma machine from the contents of configuration
     *  file _config and apply it to the messages in _input, sending the
     *  results to _output. */
    private void process() {
        Machine m = readConfig();
        String line = "";
        setUp(m, _input.nextLine());
        while (_input.hasNextLine()) {
            String temp = _input.nextLine();
            if (temp.contains("*")) {
                setUp(m, temp);
            } else {
                printMessageLine(m.convert(temp.replaceAll(" ", "")));
            }

        }
    }

    /** Return an Enigma machine configured from the contents of configuration
     *  file _config. */
    private Machine readConfig() {
        try {
            String str = _config.next();
            if (str.contains("(") || str.contains(")") || str.contains("*")) {
                throw new EnigmaException("Wrong config format");
            }
            _alphabet = new Alphabet(str);
            int numRotors = _config.nextInt();
            int pawls = _config.nextInt();
            _allRotors = new ArrayList<>();
            while (_config.hasNext()) {
                _allRotors.add(readRotor());
            }
            return new Machine(_alphabet, numRotors, pawls, _allRotors);
        } catch (NoSuchElementException excp) {
            throw error("configuration file truncated");
        }
    }

    /** Return a rotor, reading its description from _config. */
    private Rotor readRotor() {
        try {
            String name = _config.next();
            String rotor = _config.next();
            String cycles = "";
            char type = rotor.charAt(0);
            while (_config.hasNext("\\(.*\\)")) {
                cycles += _config.next();
            }

            String notch = "";
            for (int i = 1; i < rotor.length(); i++) {
                notch += rotor.charAt(i);
            }
            Permutation perm = new Permutation(cycles, _alphabet);
            if (type == 'M') {
                return new MovingRotor(name, perm, notch);
            } else if (type == 'N') {
                return new FixedRotor(name, perm);
            } else if (type == 'R') {
                return new Reflector(name, perm);
            } else {
                throw new EnigmaException("no rotor");
            }
        } catch (NoSuchElementException excp) {
            throw error("bad rotor description");
        }
    }

    /** Set M according to the specification given on SETTINGS,
     *  which must have the format specified in the assignment. */
    private void setUp(Machine M, String settings) {
        String[] set = settings.split(" ");
        if (!set[0].equals("*")) {
            throw new EnigmaException("must begin with *");
        }
        if (set.length - 2 < M.numRotors()) {
            throw new EnigmaException("wrong number of rotors");
        }

        String temp = set[M.numRotors() + 1];
        if (temp.length() != M.numRotors() - 1) {
            throw new EnigmaException("arguments wrong");
        }
        for (int i = 0; i < temp.length(); i++) {
            if (!_alphabet.contains(temp.charAt(i))) {
                throw new EnigmaException("wrong setting format 2");
            }
        }

        int moving = 0;
        for (int i = 1; i < set.length - 1; i++) {
            String s = set[i];
            if (!(s.equals("Beta") | s.equals("Gamma") | s.equals("B"))) {
                moving += 1;
            }
        }
        if (moving != 3) {
            throw new EnigmaException("wrong number of moving rotors");
        }


        String[] rotors = new String[M.numRotors()];
        for (int i = 1; i < M.numRotors() + 1; i++) {
            rotors[i - 1] = set[i];
        }

        for (int i = 0; i < rotors.length - 1; i++) {
            for (int j = i + 1; j < rotors.length; j++) {
                if (rotors[i].equals(rotors[j])) {
                    throw new EnigmaException("Repeated Rotor");
                }
            }
        }

        String[] rotor = new String[M.numRotors()];
        String cycles = "";
        for (int i = 0; i < rotor.length; i++) {
            rotor[i] = set[i + 1];
        }
        for (int i = M.numRotors() + 2; i < set.length; i++) {
            cycles += set[i];
        }
        Permutation perm = new Permutation(cycles, _alphabet);
        M.setPlugboard(perm);
        M.insertRotors(rotors);
        M.setRotors(set[M.numRotors() + 1]);
    }

    /** Print MSG in groups of five (except that the last group may
     *  have fewer letters). */
    private void printMessageLine(String msg) {
        String result = "";
        for (int i = 0; i < msg.length(); i += 5) {
            if (msg.length() - i <= 5) {
                result += msg.substring(i);
            } else {
                result += msg.substring(i, i + 5) + " ";
            }
        }
        _output.println(result);
    }

}
