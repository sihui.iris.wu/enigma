package enigma;

import java.util.Collection;

import static enigma.EnigmaException.*;

/** Class that represents a complete enigma machine.
 *  @author Iris Wu
 */
class Machine {
    /** Common alphabet of my rotors. */
    private final Alphabet _alphabet;

    /** Number of rotors in the machine. */
    private int _numRotors;

    /** Number of pawls I have. */
    private int _pawls;

    /** Collection of rotors I have. */
    private Object[] _allRotors;

    /** The permutation of plugboard that I have. */
    private Permutation _plugboard;

    /** An array that contains the rotor in the machine. */
    private Rotor[] _rotors;

    /** A new Enigma machine with alphabet ALPHA, 1 < NUMROTORS rotor slots,
     *  and 0 <= PAWLS < NUMROTORS pawls.  ALLROTORS contains all the
     *  available rotors. */
    Machine(Alphabet alpha, int numRotors, int pawls,
            Collection<Rotor> allRotors) {
        _numRotors = numRotors;
        _alphabet = alpha;
        _pawls = pawls;
        _allRotors = allRotors.toArray();
        _rotors = new Rotor[_numRotors];
    }

    /** Return the number of rotor slots I have. */
    int numRotors() {
        return _numRotors;
    }

    /** Return the number pawls (and thus rotating rotors) I have. */
    int numPawls() {
        return _pawls;
    }

    /** Set my rotor slots to the rotors named ROTORS from my set of
     *  available rotors (ROTORS[0] names the reflector).
     *  Initially, all rotors are set at their 0 setting. */
    void insertRotors(String[] rotors) {
        for (int i = 0; i < rotors.length; i++) {
            for (int j = 0; j < _allRotors.length; j++) {
                String from = rotors[i].toString();
                String to = (((Rotor) _allRotors[j]).name());
                if ((from).equals(to)) {
                    _rotors[i] = (Rotor) _allRotors[j];
                }
            }
        }
        if (_rotors.length != rotors.length) {
            throw new EnigmaException("length was not equal");
        }
    }

    /** Set my rotors according to SETTING, which must be a string of
     *  numRotors()-1 characters in my alphabet. The first letter refers
     *  to the leftmost rotor setting (not counting the reflector).  */
    void setRotors(String setting) {
        if (setting.length() != _numRotors - 1) {
            throw new EnigmaException("wrong string length");
        }
        for (int i = 1; i < _rotors.length; i++) {
            if (!_alphabet.contains(setting.charAt(i - 1))) {
                throw new EnigmaException("string does not belong to alphabet");
            }
            _rotors[i].set(setting.charAt(i - 1));
        }
    }

    /** Set the plugboard to PLUGBOARD. */
    void setPlugboard(Permutation plugboard) {
        _plugboard = plugboard;
    }

    /** Returns the result of converting the input character C (as an
     *  index in the range 0..alphabet size - 1), after first advancing
     *  the machine. */
    int convert(int c) {
        boolean[] booList = new boolean[numRotors()];
        for (int i = 0; i < numRotors(); i++) {
            if (i == numRotors() - 1) {
                booList[i] = true;
            } else if (!_rotors[i].rotates()) {
                booList[i] = false;
            } else if (_rotors[i + 1].atNotch()) {
                booList[i] = booList[i + 1] = true;
            }
        }

        for (int j = 0; j < numRotors(); j++) {
            if (booList[j]) {
                _rotors[j].advance();
            }
        }

        int result = _plugboard.permute(c);
        for (int k = _rotors.length - 1; k >= 0; k--) {
            result = _rotors[k].convertForward(result);
        }
        for (int l = 1; l < _rotors.length; l++) {
            result = _rotors[l].convertBackward(result);
        }
        result = _plugboard.permute(result);
        return result;
    }


    /** Returns the encoding/decoding of MSG, updating the state of
     *  the rotors accordingly. */
    String convert(String msg) {
        String result = "";
        for (int i = 0; i < msg.length(); i++) {
            int dest = convert(_alphabet.toInt(msg.charAt(i)));
            char temp = _alphabet.toChar(dest);
            result += temp;
        }
        return result;
    }


}
