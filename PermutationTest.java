package enigma;

import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.Timeout;
import static org.junit.Assert.*;

import static enigma.TestUtils.*;

/** The suite of all JUnit tests for the Permutation class.
 *  @author Iris Wu
 */
public class PermutationTest {

    /** Testing time limit. */
    @Rule
    public Timeout globalTimeout = Timeout.seconds(5);

    /* ***** TESTING UTILITIES ***** */

    private Permutation perm;
    private String alpha = UPPER_STRING;

    /** Check that perm has an alphabet whose size is that of
     *  FROMALPHA and TOALPHA and that maps each character of
     *  FROMALPHA to the corresponding character of FROMALPHA, and
     *  vice-versa. TESTID is used in error messages. */
    private void checkPerm(String testId,
                           String fromAlpha, String toAlpha) {
        int N = fromAlpha.length();
        assertEquals(testId + " (wrong length)", N, perm.size());
        for (int i = 0; i < N; i += 1) {
            char c = fromAlpha.charAt(i), e = toAlpha.charAt(i);
            assertEquals(msg(testId, "wrong translation of '%c'", c),
                         e, perm.permute(c));
            assertEquals(msg(testId, "wrong inverse of '%c'", e),
                         c, perm.invert(e));
            int ci = alpha.indexOf(c), ei = alpha.indexOf(e);
            assertEquals(msg(testId, "wrong translation of %d", ci),
                         ei, perm.permute(ci));
            assertEquals(msg(testId, "wrong inverse of %d", ei),
                         ci, perm.invert(ei));
        }
    }

    /* ***** TESTS ***** */

    @Test
    public void checkIdTransform() {
        perm = new Permutation("", UPPER);
        checkPerm("identity", UPPER_STRING, UPPER_STRING);
    }

    /*
    @Test
    public void checkCycle() {
        Permutation perm = new Permutation("(BCDA)", new Alphabet("ABCD"));
        checkPerm("test1", "ABCD", "BCDA");
        //Permutation perm = new Permutation("(AD)(BC)", new Alphabet("ABCD"));
        checkPerm("test2", "BDCA", "CABD");
    }
     */
    @Test
    public void sizeTest() {
        Permutation case1 = new Permutation("(ABCD)", UPPER);
        Permutation case2 = new Permutation("", UPPER);
        Permutation case3 = new Permutation("(ABCD)", UPPER);

        assertEquals(26, case1.size());
        assertEquals(26, case2.size());
        assertEquals(26, case3.size());
    }

    @Test
    public void permuteCharTest() {
        Permutation case1 = new Permutation("(DCBA)", UPPER);
        assertEquals('C', case1.permute('D'));
        assertEquals('B', case1.permute('C'));
        assertEquals('A', case1.permute('B'));
        assertEquals('D', case1.permute('A'));

        Permutation case2 = new Permutation("(DC)", UPPER);
        assertEquals('C', case2.permute('D'));
        assertEquals('B', case2.permute('B'));
        assertEquals('A', case2.permute('A'));

    }

    @Test
    public void permuteIntTest() {
        Permutation case1 = new Permutation("(BA)", UPPER);
        assertEquals(0, case1.permute(1));
        assertEquals(1, case1.permute(0));

        Permutation case2 = new Permutation("(ABC)(DE)", UPPER);
        assertEquals(1, case2.permute(0));
        assertEquals(0, case2.permute(2));

    }

    @Test
    public void invertCharTest() {
        Permutation case1 = new Permutation("(DCBA)", UPPER);
        assertEquals('D', case1.invert('C'));
        assertEquals('C', case1.invert('B'));
        assertEquals('B', case1.invert('A'));

        Permutation case2 = new Permutation("(ABC)(EDF)", UPPER);
        assertEquals('A', case2.invert('B'));
        assertEquals('E', case2.invert('D'));
        assertEquals('G', case2.invert('G'));
    }

    @Test
    public void invertIntTest() {
        Permutation case1 = new Permutation("(ABCD)", UPPER);
        assertEquals(0, case1.invert(1));
        assertEquals(2, case1.invert(3));
        assertEquals(1, case1.invert(2));


        Permutation case2 = new Permutation("(ABC)(EDF)", UPPER);
        assertEquals(0, case2.invert(1));
        assertEquals(1, case2.invert(2));
    }

    @Test
    public void derangementTest() {
        Permutation case1 = new Permutation("(ABCD)", UPPER);
        Permutation case2 = new Permutation("(AB)", UPPER);
        assertFalse(case1.derangement());
        assertFalse(case2.derangement());
    }

    @Test
    public void indexTest() {
        String cycles = "(ABCDEFGHIJKLMNOPQRSTUVXYZ)";
        Permutation case1 = new Permutation(cycles, UPPER);
        assertEquals(2, case1.permute(27));
        assertEquals(3, case1.permute(2));
        assertEquals(4, case1.invert(31));
    }

}
